module.exports = function (skip, limit){
    let limit_item = parseInt(limit);
    let skip_item = (parseInt(skip) - 1) * limit;
    return {
        limit: limit_item,
        skip: skip_item
    }
};