const moment = require("moment");
const constants = require("./../utilities/Constants");

module.exports = {
    generalId: function (lengthString) {
        let text = "";
        const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
        for (var i = 0; i < lengthString; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }
};