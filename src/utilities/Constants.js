module.exports = {
    "FORMAT_FULL_DATE": "DD-MM-YYYY HH:mm:ss",
    "FORMAT_DATE": "DD-MM-YYYY",
    "NOT_FORMAT_FULL_DATE": "DDMMYYYYHHmmss",
    "SALT": "u$3r_cR347E",
    "IS_TRUE": true,
    "IS_FALSE": false,
};