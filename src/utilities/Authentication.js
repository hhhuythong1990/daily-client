const bcrypt = require("bcryptjs");
const messageResponse = require("./../utilities/MessageResponse");

module.exports = function(req, res, next){
    const userSession = req.header("I-API-CLIENT");
    if(!userSession){
        res.json(messageResponse().MISSING_HEADER_FIELD);
    }else{
        let salt = "D@i1y-c1I3n7-P4$$w0rK";
        // let lenghtCode = bcrypt.genSaltSync(10);
        // let code = bcrypt.hashSync(salt, lenghtCode);
        // console.log(code);
        // $2a$10$6HOnRyM1M5vsTFyi0DDW0O0G196spbOXz19aFZmjuBzAG1WdypKde
        let checkToken = bcrypt.compareSync(salt, userSession);
        if(checkToken){
            next();
        }else{
            res.json(messageResponse().WRONG_TOKEN_KEY);
        }
    }
}