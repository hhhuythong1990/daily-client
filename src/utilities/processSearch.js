const moment = require("moment");
const mongoose = require("mongoose");
const constants = require("./Constants");
module.exports = {
    searchGiftCode: (dataSearch) => {
        let query = { "$and": [] };
        let agency = dataSearch.agency;
        let dateStart = dataSearch.date_start;
        let dateEnd = dataSearch.date_end;
        let code = dataSearch.code;
        let start_time, end_time, agencyId, codeQuery;
        
        if(agency != null){
            agencyId = { "agency": mongoose.Types.ObjectId(agency) };
            query.$and.push(agencyId);
        }
        if(code != null){
            codeQuery = { "pin_code": new RegExp(code, 'i') };
            query.$and.push(codeQuery);
        }
        if(dateStart != null){
            let startTimeEpoch = moment(dateStart, constants.FORMAT_FULL_DATE).valueOf();
            start_time = {"day_create_epoch" : {$gte: startTimeEpoch}}
            query.$and.push(start_time);
        }
        if(dateEnd != null){
            let endTimeEpoch = moment(dateEnd, constants.FORMAT_FULL_DATE).valueOf();
            end_time = {"day_create_epoch" : {$lte: endTimeEpoch}}
            query.$and.push(end_time);
        }
        return query;
    },
    searchTransaction: (dataSearch) => {
        let andQuery = { "$and": [] };
        let agencyName = dataSearch.agency_name;
        let dateStart = dataSearch.date_start;
        let dateEnd = dataSearch.date_end;
        let start_time, end_time;
        
        if(agencyName != null){
            agencyNameQuery = { "agency_name": new RegExp(agencyName, 'i') };
            andQuery.$and.push(agencyNameQuery);
            query = andQuery;
        }
        
        if(dateEnd != null){
            let endTimeEpoch = moment(dateEnd, constants.FORMAT_FULL_DATE).valueOf();
            end_time = {"time_create_epoch" : {$lte: endTimeEpoch}}
            andQuery.$and.push(end_time);
            query = andQuery;
        }
        if(dateStart != null){
            let startTimeEpoch = moment(dateStart, constants.FORMAT_FULL_DATE).valueOf();
            start_time = {"time_create_epoch" : {$gte: startTimeEpoch}}
            andQuery.$and.push(start_time);
            query = andQuery;
        }
        return query;
    }
}