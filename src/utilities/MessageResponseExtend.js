/**
 * Created by User on 6/16/2017.
 */
module.exports = function (name, data, total = null, errCode){
    const OMR = require("./MessageResponse");
    let OMR_EXTEND =  OMR(name, data, total);
    let mappingData = {  3 : "QUERY_ERROR", 11000 :  "EXIST_OBJECT"};

    if( (errCode === null) || (typeof errCode === "undefined")){    
        errCode = 3;
    }
    return OMR_EXTEND[mappingData[errCode]];
}