const moment = require("moment");
const async = require("async");
const md5 = require("md5");
const _ = require("lodash");

const messageValidate = require("./../utilities/MessageValidate");
const messageResponse = require("./../utilities/MessageResponse");
const constants =  require("./../utilities/Constants");

const Profile = require("./../model/Profile");
const Package = require("./../model/Package");
const Agency = require("./../model/Agency");
const AgencyPackage = require("./../model/AgencyPackage");

exports.getBySkipLimit = (req,res) => {
    req.check("skip", messageValidate("skip")).notEmpty();
    req.check("limit", messageValidate("limit")).notEmpty();
    req.check("agency", messageValidate("agency")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            let agencyId = req.query.agency;
            AgencyPackage.sum(agencyId, (err, totalData) => {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(totalData.length == 0){
                        res.json(messageResponse(null, null).NOT_FOUND);
                    }else{
                        AgencyPackage.getAll(req.query.skip, req.query.limit, agencyId, (err, agencyPackage) => {
                            if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            }else{
                                res.json(messageResponse(null, agencyPackage, totalData).FOUND_DATA);
                            }
                        });
                    }
                }
            });
        }
    }).catch(function(rea){
        console.log(rea);
    });
}

exports.getByDataSearchSkipLimit = (req, res) => {
	req.check("limit", messageValidate("limit")).notEmpty();
    req.check("skip", messageValidate("skip")).notEmpty();
    req.check("agencyPackageName", messageValidate("agencyPackageName")).notEmpty();
    req.check("agency", messageValidate("agency")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let dataSearch = decodeURI(req.query.agencyPackageName);
            let agencyId = req.query.agency;
            AgencyPackage.takeDataSearchSkipLimit(req.query.skip, req.query.limit, dataSearch, agencyId, (err, dataList)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{                    
                    AgencyPackage.sumQuery(dataSearch, agencyId, (err, data)=>{
                        if(err){
                            res.json(messageResponse(err).QUERY_ERROR);
                        }else{
                            res.json(messageResponse(null, dataList, data).FOUND_DATA);
                        }
                    });
                }
            });
    	}
    });
}

exports.getAgencyPackageById = (req, res) => {
    req.check("agency_package_id", messageValidate("agency_package_id")).notEmpty();
    req.check("agency", messageValidate("agency")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            AgencyPackage.takeAgencyPackageById(req.query.agency_package_id, req.query.agency, (err, agencyPackage) => {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(!agencyPackage){
                        return res.json(messageResponse(null, null).NOT_FOUND);
                    }else{
                        res.json(messageResponse(null, agencyPackage, null).FOUND_DATA);                       
                    }
                }
            });
        }
    }).catch(function(rea){
        console.log(rea);
    });
}