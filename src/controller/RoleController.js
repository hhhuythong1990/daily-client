const express = require("express");
const router = express.Router();
const moment = require("moment");
const async = require("async");

const messageValidate = require("./../utilities/MessageValidate");
const messageResponse = require("./../utilities/MessageResponse");
const constants =  require("./../utilities/Constants");
const Role = require("./../model/Role");


exports.create = (req, res) =>{
    req.check("role_name", messageValidate("role_name")).notEmpty();
    req.check("permissions", messageValidate("permissions")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let now = new Date();
            let newRole = new Role({
                "role_name" :  req.body.role_name,
                "permissions" :  req.body.permissions,
                "date_create"  :  now,
                "date_create_string"  :  moment(now).format(constants.FORMAT_FULL_DATE)
            });
            Role.insert(newRole,(err,data)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    res.json(messageResponse("Vai trò", data).ADD_SUCCESSFULLY);   
                }
            });
        }
    });
}
exports.getAll = (req, res) => {
	req.check("limit", messageValidate("limit")).notEmpty();
    req.check("skip", messageValidate("skip")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            Role.getAll(req.body.skip, req.body.limit,(err,datalist)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    Role.sum((err,data)=>{
                        if(err){
                            res.json(messageResponse(err).QUERY_ERROR);
                        }else{
                            res.json(messageResponse(null, datalist, data).FOUND_DATA);
                        }
                    });
                }
            });
    	}
    });
}

exports.remove = (req, res) => {
	req.check("role_id", messageValidate("role_id")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    	    res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
			Role.deleteById(req.body.role_id,(err,resd)=>{
				if(err){
					res.json(messageResponse(err).QUERY_ERROR);
				}else if(resd.result.n) { 
					res.json(messageResponse(null, null).DELETE_SUCCESSFULLY);
				}else{
                    res.json(messageResponse(err).NOT_FOUND);
                }
			})
    	}
    });
}

exports.update = (req, res) => {
	req.check("role_id", messageValidate("role_id")).notEmpty();
	req.check("role_name", messageValidate("role_name")).notEmpty();
    req.check("permissions", messageValidate("permissions")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
			let dataUpdate = {
                "permissions" :  req.body.permissions,
                "role_name"  :  req.body.role_name,
                "date_update"  : Date(),
                "date_update_string" : moment(Date()).format(constants.FORMAT_FULL_DATE)
            };
			Role.updateByeId(req.body.role_id,dataUpdate,(err, role)=>{
				if(err){
					res.json(messageResponse(err).QUERY_ERROR);
				} else if (role.n !== 0){
					res.json(messageResponse(null, role).UPDATE_SUCCESSFULLY);
				} else {
					res.json(messageResponse(err).NOT_FOUND);
				}
			});
		}
	});
}