const express = require("express");
const router = express.Router();
const moment = require("moment");
const async = require("async");
const md5 = require("md5");
const _ = require("lodash");

const messageValidate = require("./../utilities/MessageValidate");
const messageResponse = require("./../utilities/MessageResponse");
const constants =  require("./../utilities/Constants");

const Profile = require("./../model/Profile");
const Agency = require("./../model/Agency");

function updateOnline(profileId, statusOnline) {
    let dataUpdate = {
        "is_online": statusOnline
    };
    Profile.updateByeId(profileId, dataUpdate, (err)=>{
        if(err){
            console.log(err)
        }
    });
}

exports.authenticationUserNamePassword = (req, res) => {
    req.check("user_name", messageValidate("user_name")).notEmpty();
	req.check("password", messageValidate("password")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
			Profile.takeProfileByUsername(req.body.user_name, (err, profile)=>{
				if(err){
					res.json(messageResponse(err).QUERY_ERROR);
				}else{                    
                    if(!profile){
                        res.json(messageResponse(null, null).LOGIN_INCORRECT);
                    }else{
                        if(profile.is_locked == true){
                            res.json(messageResponse("Rất tiếc tài khoản của bạn đã bị khóa", null).CUSTOM_MESSAGE);
                        }else {
                            let password = md5(req.body.password + constants.SALT);
                            if(profile.password == password){
                                if(profile.is_online == true){
                                    res.json(messageResponse("Tài khoản đã được đăng nhập", null).CUSTOM_MESSAGE);
                                }else {
                                    updateOnline(profile._id, true);
                                    res.json(messageResponse(null, profile).LOGIN_SUCCESSFULLY);
                                }
                            }else{
                                res.json(messageResponse(null, null).LOGIN_INCORRECT);
                            }
                        }
                    }
				}
			})
    	}
    });
}

exports.logout = (req, res) => {
    req.check("profile_id", messageValidate("profile_id")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            updateOnline(req.body.profile_id, false);
            res.json(messageResponse(null, null).CUSTOM_MESSAGE);
        }
    });
}

exports.changePassword = (req, res) => {
    req.check("user_id", messageValidate("user_id")).notEmpty();
    req.check("old_password", messageValidate("old_password")).notEmpty();
    req.check("new_password", messageValidate("new_password")).notEmpty();
    req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let profileId = req.body.user_id;
            Profile.takeProfileById(profileId, (err, profile)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(profile){
                        let oldPassword = md5(req.body.old_password + constants.SALT);
                        if(profile.password == oldPassword){
                            let dataUpdate = {
                                "password": md5(req.body.new_password + constants.SALT)
                            };
                            Profile.updateByeId(profileId, dataUpdate, (err, result)=>{
                                if(err){
                                    res.json(messageResponse(err).QUERY_ERROR);
                                } else{
                                    res.json(messageResponse("cập nhật mật khẩu", null).UPDATE_SUCCESSFULLY);
                                }
                            });
                        }else{
                            res.json(messageResponse("mật khẩu cũ không đúng", null).CUSTOM_MESSAGE);
                        }
                    }else {
                        res.json(messageResponse("", "").NOT_FOUND)
                    }
                }
            });
    	}
    });
} 

exports.changeOnline = (req, res) => {
    req.check("profile_id", messageValidate("profile_id")).notEmpty();
    req.check("is_online", messageValidate("is_online")).notEmpty();
    req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
            
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let profileId = req.body.profile_id;
            let dataUpdate = {
                "is_online":  (req.body.is_online == "true")?true:false,
            };
            Profile.updateByeId(profileId, dataUpdate, (err, result)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                } else{
                    res.json(messageResponse("người dùng",null).UPDATE_SUCCESSFULLY);
                }
            });
        }
    });
}

exports.takeProfileChange = (req, res) => {
    req.check("profile_id", messageValidate("profile_id")).notEmpty();
    req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let profileId = req.body.profile_id;
            Profile.takeProfileChange(profileId, true, (err, profileChanged)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                } else{
                    if(!profileChanged){
                        res.json(messageResponse(null, null).CHECK_CHANGED);
                    }else {
                        if(profileChanged.is_locked == true){
                            let dataResponse = {
                                "locked": true,
                                "dataUpdate": null
                            }
                            updateChangedProfile(profileId);
                            return res.json(messageResponse(null, dataResponse).CHECK_CHANGED);
                        }else {
                            Profile.takeProfileById(profileId, (err, profileInfo)=>{
                                if(err){
                                    res.json(messageResponse(err).QUERY_ERROR);
                                }else {
                                    let dataResponse = {
                                        "locked": null,
                                        "dataUpdate": profileInfo
                                    }
                                    updateChangedProfile(profileId);
                                    return res.json(messageResponse(null, dataResponse).CHECK_CHANGED);
                                }
                            });
                        }
                    }
                }
            });
        }
    });
}

function updateChangedProfile(profileId){
    let dataUpdate = {
        "is_changed": true,
    };
    Profile.updateByeId(profileId, dataUpdate);
}

exports.changePassword2 = (req, res) => {
    req.check("user_id", messageValidate("user_id")).notEmpty();
    req.check("password_2", messageValidate("password_2")).notEmpty();
    req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let profileId = req.body.user_id;
            Profile.takeProfileById(profileId, (err, profile)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(profile){
                        let dataUpdate = {
                            "password_2": md5(req.body.password_2 + constants.SALT)
                        };
                        let agencyId = profile.is_agency._id;
                        Agency.updateByeId(agencyId, dataUpdate, (err, result)=>{
                            if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            } else{
                                updateChangedProfile(profileId);
                                res.json(messageResponse("cập nhật mật khẩu", null).UPDATE_SUCCESSFULLY);
                            }
                        });
                    }else {
                        res.json(messageResponse("", "").NOT_FOUND)
                    }
                }
            });
    	}
    });
} 