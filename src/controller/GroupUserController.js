const moment = require("moment");
const async = require("async");

const messageValidate = require("./../utilities/MessageValidate");
const messageResponse = require("./../utilities/MessageResponse");
const constants =  require("./../utilities/Constants");

const GroupUser = require("./../model/GroupUser");
exports.create = (req, res) => {
    req.check("group_user_name", messageValidate("group_user_name")).notEmpty();
    req.check("group_user_name_slug", messageValidate("group_user_name_slug")).notEmpty();
    req.check("publish", messageValidate("publish")).notEmpty();
    req.check("user_create", messageValidate("user_create")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            let groupUserName = req.body.group_user_name;
            GroupUser.takeGroupUserByName(groupUserName, (err, existGroupName) => {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(existGroupName){
                        res.json(messageResponse("Tên nhóm người dùng", null).EXIST_OBJECT);
                    }else{
                        let now = new Date();
                        let newGroupUser = new GroupUser ({
                            group_user_name: groupUserName,
                            is_publish: req.body.publish,
                            group_user_name_slug: req.body.group_user_name_slug,
                            "user_create": req.body.user_create,
                            day_create_date: now,
                            day_create_string: moment(now).format(constants.FORMAT_FULL_DATE),
                        });
                        GroupUser.insertGroupUser(newGroupUser, (err, groupUser) => {
                            if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            }else{                 
                                res.json(messageResponse("tên nhóm người dùng", null).ADD_SUCCESSFULLY);                        
                            }
                        });
                    }
                }
            });
        }
    }).catch((rea) => {
        res.json(rea);
    });
};

exports.getBySkipLimit = (req, res) =>{
    req.check("skip", messageValidate("skip")).notEmpty();
    req.check("limit", messageValidate("limit")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            GroupUser.takeBySkipLimit(req.body.skip, req.body.limit,(err,datalist)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    GroupUser.sum((err,data)=>{
                        if(err){
                            res.json(messageResponse(err).QUERY_ERROR);
                        }else{
                            res.json(messageResponse(null, datalist, data).FOUND_DATA);
                        }
                    });
                }
            });

        }
    });
}

exports.remove = (req, res) => {
	req.check("group_user_id", messageValidate("group_user_id")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    	    res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
			GroupUser.deleteById(req.body.group_user_id,(err,resd)=>{
				if(err){
					res.json(messageResponse(err).QUERY_ERROR);
				}else if(resd.result.n) { 
					res.json(messageResponse(null, null).DELETE_SUCCESSFULLY);
				}else{
                    res.json(messageResponse(err).NOT_FOUND);
                }
			})
    	}
    });
}

exports.update = (req, res) => {
    req.check("group_user_id", messageValidate("group_user_id")).notEmpty();
    req.check("group_user_name", messageValidate("group_user_name")).notEmpty();
    req.check("user_update", messageValidate("user_update")).notEmpty();    
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let now = new Date();
			let dataUpdate = {
                "group_user_name" : req.body.group_user_name,
                "day_update_date" : now,
                "user_update": req.body.user_update,
                "day_update_string" : moment(now).format(constants.FORMAT_FULL_DATE)
            };
			GroupUser.updateByeId(req.body.group_user_id,dataUpdate,(err, resd)=>{
				if(err){
					res.json(messageResponse(err).QUERY_ERROR);
				} else if (resd.n !== 0){
					res.json(messageResponse(null,resd).UPDATE_SUCCESSFULLY);
				} else {
					res.json(messageResponse(err).NOT_FOUND);
				}

			});
		}
	});
}

exports.getGroupAdmin = (req, res) => {
    req.check("super_id", messageValidate("super_id")).notEmpty();   
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            GroupUser.getGroupAdmin(req.body.super_id, (err, groupAdmin)=>{
                if(err){
					res.json(messageResponse(err).QUERY_ERROR);
				} else {
                    if(!groupAdmin){
                        res.json(messageResponse(err).NOT_FOUND);
                    }else {
                        res.json(messageResponse("", groupAdmin).FOUND_DATA);
                    }
				}
            });
		}
	});
}