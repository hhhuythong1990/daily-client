// const async = require("async");
const moment = require("moment");

const messageValidate = require("./../utilities/MessageValidate");
const messageResponse = require("./../utilities/MessageResponse");
const constants =  require("./../utilities/Constants");

const GroupModule = require("./../model/GroupModule");

exports.create = (req, res) => {
	req.check("group_module_name", messageValidate("group_module_name")).notEmpty();
	req.check("modules", messageValidate("modules")).notEmpty();
	req.check("group_name_slug", messageValidate("group_name_slug")).notEmpty();
	req.check("publish", messageValidate("publish")).notEmpty();
	req.check("user_create", messageValidate("user_create")).notEmpty();
    req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
			let groupModuleName = req.body.group_module_name;
			GroupModule.takeGroupModuleByGroupName(groupModuleName, (err, groupModule) => {
				if(err){
					res.json(messageResponse(err).QUERY_ERROR);
				}else {
					if(groupModule){
						res.json(messageResponse("Nhóm mô-dun", moduleName).EXIST_OBJECT);
					}else {
						let now = new Date();
						let newGroupModule = new GroupModule({
							"group_name" : groupModuleName,
							"modules" : req.body.modules,
							"group_name_slug": req.body.group_name_slug,
							"is_publish": req.body.publish,
							"user_create": req.body.user_create,
							"day_create_date": now,
							"day_create_string": moment(now).format(constants.FORMAT_FULL_DATE),

						});
						GroupModule.insertGroupModule(newGroupModule,(err,resd)=>{
							if(err){
								res.json(messageResponse(err).QUERY_ERROR);
							} else {
								res.json(messageResponse("nhóm mô-dun", resd).ADD_SUCCESSFULLY);    
							}
						});
					}
				}
			});
    	}
    });
}

exports.getBySkipLimit = (req, res) => {
	req.check("limit", messageValidate("limit")).notEmpty();
	req.check("skip", messageValidate("skip")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
			GroupModule.takeGroupModuleBySkipLimit(req.query.skip, req.query.limit,(err, datalist)=>{
				if(err){
					res.json(messageResponse(err).QUERY_ERROR);
				}else{
					GroupModule.sumModules((err,data)=>{
						if(err){
							res.json(messageResponse(err).QUERY_ERROR);
						}else{
							res.json(messageResponse(null, datalist, data).FOUND_DATA);
						}
					});
				}
			});
    	}
    });
}

exports.getByDataSearchSkipLimit = (req, res) => {
	req.check("limit", messageValidate("limit")).notEmpty();
	req.check("skip", messageValidate("skip")).notEmpty();
	req.check("groupModuleName", messageValidate("groupModuleName")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
			console.log(result.array());
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
			let dataSearch = decodeURI(req.query.groupModuleName);
			GroupModule.takeDataSearchBySkipLimit(req.query.skip, req.query.limit, dataSearch, (err, datalist)=>{
				if(err){
					res.json(messageResponse(err).QUERY_ERROR);
				}else{
					GroupModule.sumQuery(dataSearch, (err,data)=>{
						if(err){
							res.json(messageResponse(err).QUERY_ERROR);
						}else{
							res.json(messageResponse(null, datalist, data).FOUND_DATA);
						}
					});
				}
			});
    	}
    });
}

exports.remove = (req, res) => {
	req.check("group_module_id", messageValidate("group_module_id")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
			GroupModule.deleteById(req.body.group_module_id,(err)=>{
				if(err){
					res.json(messageResponse(err).QUERY_ERROR);
				}else{ 
					res.json(messageResponse(null, null).DELETE_SUCCESSFULLY);
				}
			})
    	}
    });
}

exports.update = (req, res) => {
	req.check("group_module_id", messageValidate("group_module_id")).notEmpty();
	req.check("group_module_name", messageValidate("group_module_name")).notEmpty();
	req.check("group_name_slug", messageValidate("group_name_slug")).notEmpty();
	req.check("modules", messageValidate("modules")).notEmpty();
	req.check("publish", messageValidate("publish")).notEmpty();
	req.check("user_update", messageValidate("user_update")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
			console.log(result.array());
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
			let groupModuleName = req.body.group_module_name;
			let groupModuleId = req.body.group_module_id;
			GroupModule.takeGroupModuleByIdAndGroupName(groupModuleId, groupModuleName, (err, groupModule) => {
				if(err){
					res.json(messageResponse(err).QUERY_ERROR);
				}else {
					if(groupModule){
						res.json(messageResponse("Nhóm mô-dun", moduleName).EXIST_OBJECT);
					}else {
						let now = Date();
						let dataUpdate = {
							"group_name" : groupModuleName,
							"group_name_slug": req.body.group_name_slug,
							"modules" : req.body.modules,
							"user_update": req.body.user_update,
							"is_publish": req.body.publish,
							"day_update_date": now,                        
							"day_update_string": moment(now).format(constants.FORMAT_FULL_DATE)
						};
						GroupModule.updateByeId(groupModuleId, dataUpdate, (err)=>{
							if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            }else{
                                res.json(messageResponse("nhóm mô-đun", null).UPDATE_SUCCESSFULLY);
                            }
						});
					}
				}
			});
    	}
    });
}

exports.getAll = (req, res) => {
	GroupModule.takeAllGroupModule((err, groupModules)=>{
		if(err){
			res.json(messageResponse(err).QUERY_ERROR);
		}else { 
			if(groupModules.length == 0){
                res.json(messageResponse(null, null).NOT_FOUND);
            }else {
                res.json(messageResponse(null, groupModules, null).FOUND_DATA);
            }
		}
	});
}
exports.getGroupModuleByIdAndGroupModuleName = (req, res) => {
    req.check("group_module_id", messageValidate("group_module_id")).notEmpty();
    req.check("group_module_name", messageValidate("group_module_name")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            GroupModule.takeGroupModuleByIdAndGroupName(req.body.group_module_id, req.body.group_module_name,(err, groupModule)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(groupModule){
                        res.json(messageResponse("", groupModule).FOUND_DATA);
                    }else {
                        res.json(messageResponse("", "").NOT_FOUND)
                    }
                }
            });
    	}
    });
}
exports.getGroupModuleByGroupModuleName = (req, res) => {
    req.check("group_module_name", messageValidate("group_module_name")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            GroupModule.takeGroupModuleByGroupName(req.body.group_module_name,(err, module)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(module){
                        res.json(messageResponse("", module).FOUND_DATA);
                    }else {
                        res.json(messageResponse("", "").NOT_FOUND)
                    }
                }
            });
    	}
    });
}

exports.getGroupModuleByGroupModuleId = (req, res) => {
    req.check("group_module_id", messageValidate("group_module_id")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            GroupModule.takeGroupModuleByGroupModuleId(req.query.group_module_id, (err, module) => {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(!module){
                        return res.json(messageResponse(null, null).NOT_FOUND);
                    }else{
                        res.json(messageResponse(null, module, null).FOUND_DATA);                       
                    }
                }
            });
        }
    }).catch(function(rea){
        console.log(rea);
    });
}