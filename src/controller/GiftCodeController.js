const axios = require('axios');
const async = require("async");
const _ = require("lodash");
const moment = require("moment");

const AgencyPackage = require("./../model/AgencyPackage");
const Profile = require("./../model/Profile");
const GiftCode = require("./../model/GiftCode");
const Agency = require("./../model/Agency");
const RequestGiftCode = require("./../model/RequestGiftCode");
const TransactionCode = require("./../model/TransactionCode");

const config = require("../config/" + (process.env["NODE_ENV"] || "production"));
const constants =  require("./../utilities/Constants");
const messageValidate = require("./../utilities/MessageValidate");
const messageResponse = require("./../utilities/MessageResponse");
const commonFunc = require("./../utilities/Common");

exports.createCode = (req, res) => {
    req.check("agency", messageValidate("agency")).notEmpty();
    req.check("agencyPackage", messageValidate("agencyPackage")).notEmpty();
    req.check("quality", messageValidate("quality")).notEmpty().isInt();
    req.check("buyForm", messageValidate("buyForm")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            let packageAgencyId = req.body.agencyPackage;
            let agencyId = req.body.agency;
            getAgencyPackageAndProfile(packageAgencyId, agencyId, (err, results) => {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    let quality = parseInt(req.body.quality);
                    if(quality > 100){
                        res.json(messageResponse("Số lượng code vượt quá định mức", null).CUSTOM_MESSAGE);
                    }else {
                        let now = new Date();
                        let useForm = req.body.buyForm;
                        let agencyPackageName = results.AgencyPackage.agency_package_name;
                        let packageName = results.AgencyPackage.package.package_name;
                        let priceAgencyPackage = results.AgencyPackage.price_after_cycle;
                        let totalPay = priceAgencyPackage * quality;
                        let paymentLimit = results.Profile.is_agency.payment_limit;
                        let original_price = results.AgencyPackage.package.package_price;
                        let cycle_price = results.AgencyPackage.cycle;
                        if(totalPay > paymentLimit){
                            res.json(messageResponse("Bạn mua code vượt quá định mức đang có", null).CUSTOM_MESSAGE);
                        }else {
                            let url = config.GIFT_CODE_API;
                            let agencyCode = results.Profile.is_agency.agency_code;
                            let dataPost = {
                                "prefix": agencyCode,
                                "quantity": quality,
                                "plan_id": results.AgencyPackage.package.plan_id
                            };
                            callAPIServer(url, dataPost, (err, response) => {
                                if(err){
                                    console.log(err);
                                    res.json(messageResponse("Lỗi kết nối API tạo code", null).CUSTOM_MESSAGE);
                                }else {
                                    let dataResponse = response.data;
                                    if(dataResponse.msg_code == "success"){
                                        let asyncData = [];
                                        _.forEach(dataResponse.msg_data.codes, function(value, index){
                                            asyncData.push(function (callback) {
                                                let codeItem = new GiftCode({
                                                    "code": value.code,
                                                    "pin_code": value.serial + commonFunc.generalId(value.code.length - value.serial.length),
                                                    "agency_package_name": agencyPackageName,
                                                    "package_name": packageName,
                                                    "price_original": original_price,
                                                    "price_after_cycle": cycle_price,
                                                    "serial": value.serial,
                                                    "plan_id": value.plan_id,
                                                    "created_date": value.created_date,
                                                    "expired_date": value.expired_date,
                                                    "primitive_data": value,
                                                    "agency": agencyId,
                                                    "use_form": useForm,
                                                    "time_create_epoch": new Date(value.created_date).getTime(),
                                                    "agency_package": packageAgencyId,
                                                    "day_create_date": now,
                                                    "day_create_string": moment(now).format(constants.FORMAT_FULL_DATE),
                                                    "user_create": agencyId,
                                                    "day_create_epoch": now.getTime()
                                                });
                                                GiftCode.insert(codeItem, (err, code) => {
                                                    callback(err, code);
                                                });
                                            });
                                        });
                                        async.parallel(asyncData, function (err, resultCode) {
                                            console.log(err);
                                            if(!err){
                                                Agency.takeAgencyById(results.Profile.is_agency._id, (err, agencyInfo)=>{
                                                    if(!err){
                                                        let newPrice = agencyInfo.payment_limit - (priceAgencyPackage * resultCode.length);
                                                        let arrayId = [];
                                                        let listGiftCode = _.forEach(resultCode, function(value, key) {
                                                            return arrayId.push(value._id);
                                                        });
                                                        let transactionId = agencyCode + commonFunc.generalId(10);

                                                        let transactionCode = new TransactionCode({
                                                            "transaction_id": transactionId,
                                                            "use_form": useForm,
                                                            "user_create": agencyId,
                                                            "agency_name": agencyCode,
                                                            "agency_package_name": agencyPackageName,
                                                            "old_payment_limit": agencyInfo.payment_limit,
                                                            "price_paid": priceAgencyPackage * resultCode.length,
                                                            "new_payment_limit": newPrice,
                                                            "amount_code_request": quality,
                                                            "amount_code_created": resultCode.length,
                                                            "price_original": original_price,
                                                            "price_cycle": cycle_price,
                                                            "price_after_cycle": priceAgencyPackage,
                                                            "price_original_paid": original_price * resultCode.length,
                                                            "price_paid_difference": (original_price * resultCode.length) - (priceAgencyPackage * resultCode.length),
                                                            "price_difference": original_price - priceAgencyPackage,
                                                            "gift_code": arrayId,
                                                            "day_create_date": now,
                                                            "time_create_epoch": now.getTime(),
                                                            "day_create_string": moment(now).format(constants.FORMAT_FULL_DATE),
                                                        });

                                                        let requestGiftCode = new RequestGiftCode({
                                                            "user_create": agencyId,
                                                            "use_form": useForm,
                                                            "agency_name": agencyCode,
                                                            "agency_package_name": agencyPackageName,
                                                            "old_payment_limit": agencyInfo.payment_limit,
                                                            "price_paid": priceAgencyPackage * resultCode.length,
                                                            "new_payment_limit": newPrice,
                                                            "amount_code_request": quality,
                                                            "amount_code_created": resultCode.length,
                                                            "price_original": original_price,
                                                            "price_cycle": cycle_price,
                                                            "price_after_cycle": priceAgencyPackage,
                                                            "price_original_paid": original_price * resultCode.length,
                                                            "price_paid_difference": (original_price * resultCode.length) - (priceAgencyPackage * resultCode.length),
                                                            "price_difference": original_price - priceAgencyPackage,
                                                            "gift_code": arrayId,
                                                            "transaction": transactionId,
                                                            "day_create_date": now,
                                                            "time_create_epoch": now.getTime(),
                                                            "day_create_string": moment(now).format(constants.FORMAT_FULL_DATE),
                                                        });
                                                        let updatePaymentLimit = { 
                                                            "payment_limit": newPrice
                                                        }
                                                        Agency.updateByeId(results.Profile.is_agency._id, updatePaymentLimit, (err, data)=>{
                                                            if(!err){
                                                                Profile.takeProfileById(agencyId, (err, profileInfo)=>{
                                                                    if(!err){
                                                                        let dataResponse = {
                                                                            profile: profileInfo,
                                                                            listCode: resultCode,
                                                                            transactionId: transactionId
                                                                        }
                                                                        TransactionCode.insert(transactionCode, (err, data)=>{
                                                                            console.log(err);
                                                                        });
                                                                        RequestGiftCode.insert(requestGiftCode, (err, data) => {
                                                                            console.log(err);
                                                                        });
                                                                        res.json(messageResponse("codes", dataResponse, null).ADD_SUCCESSFULLY);
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                }
            });
        }
    });
}

function callAPIServer(url, data = null, callback){
    let options = {
        // live
        headers: { "APP-ID": "APP0035", "APP-KEY": "4e3934e9ae0434f41b2559214a82e1116a9162e9", "Content-Type":"application/json" }   
        // dev
        // headers: { "APP-ID": "APP0013", "APP-KEY": "64634fd2ac0ae155eb45b46eb1aeb8b93c976b08", "Content-Type":"application/json" }
    }
    if(data != null){
        axios.post(url, data, options)
        .then(function (response) {
            callback(null, response);
        })
        .catch(function (error) {
            callback(error, null)
        });
    }else {
        axios.get(url, options)
        .then(function (response) {
            callback(null, response);
        })
        .catch(function (error) {
            callback(error, null)
        });
    }
    
}

function getAgencyPackageAndProfile(agencyPackage, agency, callback) {
    async.parallel({        
        AgencyPackage: function (callback) {
            AgencyPackage.takeAgencyPackageById(agencyPackage, agency, callback);
        },
        Profile: function (callback) {
            Profile.takeModuleByProfileId(agency, callback);
        },
    }, function (err, results) {
        if(err){
            callback(err, null);
        }else{
            callback(null, results);
        }
    });
}

exports.queryDataServer = (req,res) => {
    req.check("agency_code", messageValidate("agency_code")).notEmpty();
    req.check("skip", messageValidate("skip")).notEmpty();
    req.check("limit", messageValidate("limit")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            let url = config.QUERY_GIFT_CODE_API;
            let agencyCode = req.body.agency_code;
            let skip = req.body.skip;
            let limit = req.body.limit;
            let paramDateFrom = req.body.date_start;
            let paramDateTo = req.body.date_end;
            let dateFrom, dateTo;
            if(paramDateFrom != null) {
                dateFrom = paramDateFrom;
            }else {
                dateFrom = "";
            }
            if(paramDateTo != null) {
                dateTo = paramDateTo;
            }else {
                dateTo = "";
            }
            url += `serial=${agencyCode}&page=${skip}&limit=${limit}&fromDate=${dateFrom}&toDate=${dateTo}&is_active=`;
            callAPIServer(url, null, (err, response) => {
                if(err){
                    console.log(err);
                    res.json(messageResponse("Lỗi kết nối API tạo code", null).CUSTOM_MESSAGE);
                }else {
                    let dataResponse = response.data;
                    if(dataResponse.msg_code == "success"){
                        res.json(messageResponse(null, response.data.msg_data.codes, response.data.msg_data.total).FOUND_DATA);
                    }
                }
                
            });
        }
    }).catch(function(rea){
        console.log(rea);
    });
}

exports.getBySkipLimit = (req,res) => {
    req.check("skip", messageValidate("skip")).notEmpty();
    req.check("limit", messageValidate("limit")).notEmpty();
    req.check("agency", messageValidate("agency")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.json(messageResponse("", result.array()).MISSING_FIELD);
        } else {
            let agencyId = req.query.agency;
            GiftCode.sum(agencyId, (err, totalData) => {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(totalData.length == 0){
                        res.json(messageResponse(null, null).NOT_FOUND);
                    }else{
                        GiftCode.getAll(req.query.skip, req.query.limit, agencyId, (err, giftCode) => {
                            if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            }else{
                                res.json(messageResponse(null, giftCode, totalData).FOUND_DATA);
                            }
                        });
                    }
                }
            });
        }
    }).catch(function(rea){
        console.log(rea);
    });
}

exports.getByDataSearchSkipLimit = (req, res) => {
	req.check("limit", messageValidate("limit")).notEmpty();
    req.check("skip", messageValidate("skip")).notEmpty();
    req.check("code_id", messageValidate("code_id")).notEmpty();
    req.check("agency", messageValidate("agency")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let dataSearch = decodeURI(req.query.code_id);
            let agencyId = req.query.agency;
            GiftCode.takeDataSearchSkipLimit(req.query.skip, req.query.limit, dataSearch, agencyId, (err, dataList)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{                    
                    GiftCode.sumQuery(dataSearch, agencyId, (err, data)=>{
                        if(err){
                            res.json(messageResponse(err).QUERY_ERROR);
                        }else{
                            res.json(messageResponse(null, dataList, data).FOUND_DATA);
                        }
                    });
                }
            });
    	}
    });
}

exports.getByDateSearchSkipLimit = (req, res) => {
	req.check("limit", messageValidate("limit")).notEmpty();
    req.check("skip", messageValidate("skip")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            queryDateSearch(req.body.skip, req.body.limit, req.body.data_search, (err, dataList)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    res.json(messageResponse(null, dataList.listCode, dataList.dataCount).FOUND_DATA);
                }
            });
    	}
    });
}

queryDateSearch = (dataSkip, dataLimit, dataSearch, callback) => {
    async.parallel({
        listCode: function (callback) {
            GiftCode.takeDateSearchSkipLimit(dataSkip, dataLimit, dataSearch, callback);
        },
        dataCount: function (callback) {
            GiftCode.sumQuery(dataSearch, callback)
        }
    }, function (err, results) {
        if(err){
            callback(err, null);
        }else{
            callback(null, results);
        }
        
    });
}

exports.exportDataCode = (req, res) => {    
    GiftCode.takeDataExport(req.body.data_search, (err, listData) => {
        if(err){
            res.json(messageResponse(err).QUERY_ERROR);
        }else{
            res.json(messageResponse(null, listData, null).FOUND_DATA);
        }
    })
}