const express = require("express");
const router = express.Router();
const moment = require("moment");
const async = require("async");

const messageValidate = require("./../utilities/MessageValidate");
const messageResponse = require("./../utilities/MessageResponse");
const constants =  require("./../utilities/Constants");
const Permission = require("./../model/Permission");


exports.create = (req, res) => {
    req.check("permission_name", messageValidate("permission_name")).notEmpty();
    req.check("permission_name_slug", messageValidate("permission_name_slug")).notEmpty();
    req.check("publish", messageValidate("publish")).notEmpty();
    req.check("user_create", messageValidate("user_create")).notEmpty();
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let now = new Date();
            let permissionName = req.body.permission_name;
            Permission.takePermissionByPermissionName(permissionName, (err, permission) => {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(permission){
                        res.json(messageResponse("Tên quyền hạn", null).EXIST_OBJECT);
                    }else {
                        let newPermission = new Permission({
                            "permission_name": permissionName,
                            "permission_name_slug": req.body.permission_name_slug,
                            "is_publish": req.body.publish,
                            "user_create": req.body.user_create,
                            "day_create_date": now,
                            "day_create_string": moment(now).format(constants.FORMAT_FULL_DATE)
                        });
                        Permission.insert(newPermission,(err,data) => {
                            if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            }else{
                                res.json(messageResponse("quyền", data).ADD_SUCCESSFULLY);   
                            }
                        });
                    }
                }
            });
        }
    });
}

exports.getAllPermission = (req, res) => {
    Permission.takeAllPermission((err, permissions) => {
        if(err){
            res.json(messageResponse(err).QUERY_ERROR);
        }else{
            if(permissions.length == 0){
                res.json(messageResponse(null, null).NOT_FOUND);
            }else {
                res.json(messageResponse(null, permissions, null).FOUND_DATA);
            }
        }
    });
}

exports.getBySkipLimit = (req, res) => {
	req.check("limit", messageValidate("limit")).notEmpty();
    req.check("skip", messageValidate("skip")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            Permission.takeBySkipLimit(req.query.skip, req.query.limit,(err,datalist)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    Permission.sum((err,data)=>{
                        if(err){
                            res.json(messageResponse(err).QUERY_ERROR);
                        }else{
                            res.json(messageResponse(null, datalist, data).FOUND_DATA);
                        }
                    });
                }
            });
    	}
    });
}

exports.getByDataSearchSkipLimit = (req, res) => {
	req.check("limit", messageValidate("limit")).notEmpty();
    req.check("skip", messageValidate("skip")).notEmpty();
    req.check("permissionName", messageValidate("permissionName")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let dataSearch = decodeURI(req.query.permissionName);
            Permission.takeByDataSearchSkipLimit(req.query.skip, req.query.limit, dataSearch, (err, dataList)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{                    
                    Permission.sumQuery(dataSearch, (err, data)=>{
                        if(err){
                            res.json(messageResponse(err).QUERY_ERROR);
                        }else{
                            res.json(messageResponse(null, dataList, data).FOUND_DATA);
                        }
                    });
                }
            });
    	}
    });
}

exports.remove = (req, res) => {
	req.check("permission_id", messageValidate("permission_id")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    	    res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
			Permission.deleteById(req.body.permission_id,(err)=>{
				if(err){
					res.json(messageResponse(err).QUERY_ERROR);
				}else { 
					res.json(messageResponse(null, null).DELETE_SUCCESSFULLY);
				}
			})
    	}
    });
}

exports.update = (req, res) => {
    req.check("permission_id", messageValidate("permission_id")).notEmpty();
	req.check("permission_name", messageValidate("permission_name")).notEmpty();
    req.check("permission_name_slug", messageValidate("permission_name_slug")).notEmpty();
    req.check("publish", messageValidate("publish")).notEmpty();
    req.check("user_update", messageValidate("user_update")).notEmpty();
	req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            let permissionId = req.body.permission_id;
            let permissionName = req.body.permission_name;
            Permission.checkPermissionByPermissionName(permissionId, permissionName, (err, permission) => {
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else {
                    if(permission){
                        res.json(messageResponse("Tên quyền hạn", null).EXIST_OBJECT);
                    }else {
                        let now = Date();
                        let dataUpdate = {
                            "permission_name" :  permissionName,
                            "permission_name_slug"  :  req.body.permission_name_slug,
                            "user_update": req.body.user_update,
                            "publish": req.body.publish,
                            "date_update_date"  : now,
                            "date_update_string" : moment(now).format(constants.FORMAT_FULL_DATE)
                        };

                        Permission.updateById(permissionId, dataUpdate, (err, permission) => {
                            if(err){
                                res.json(messageResponse(err).QUERY_ERROR);
                            } else {
                                res.json(messageResponse("quyền hạn", null).UPDATE_SUCCESSFULLY);
                            } 
                        });
                    }
                }
            });
        }
    });
}

exports.getPermissionById = (req, res) => {
	req.check("permission_id", messageValidate("permission_id")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            Permission.takePermissionById(req.query.permission_id ,(err, permission)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(permission){
                        res.json(messageResponse("", permission).FOUND_DATA);
                    }else {
                        res.json(messageResponse("", "").NOT_FOUND)
                    }
                }
            });
    	}
    });
}

exports.getPermissionByIdAndPermissionName = (req, res) => {
    req.check("permission_id", messageValidate("permission_id")).notEmpty();
    req.check("permission_name", messageValidate("permission_name")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            Permission.checkPermissionByIdAndPermissionName(req.body.permission_id, req.body.permission_name,(err, permission)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(permission){
                        res.json(messageResponse("", permission).FOUND_DATA);
                    }else {
                        res.json(messageResponse("", "").NOT_FOUND)
                    }
                }
            });
    	}
    });
}

exports.getPermissionByPermissionName = (req, res) => {
    req.check("permission_name", messageValidate("permission_name")).notEmpty();
	req.getValidationResult().then((result) => {
    	if (!result.isEmpty()) {
    		res.json(messageResponse("", result.array()).MISSING_FIELD);
    	} else {
            Permission.takePermissionByPermissionName(req.body.permission_name,(err, permission)=>{
                if(err){
                    res.json(messageResponse(err).QUERY_ERROR);
                }else{
                    if(permission){
                        res.json(messageResponse("", permission).FOUND_DATA);
                    }else {
                        res.json(messageResponse("", "").NOT_FOUND)
                    }
                }
            });
    	}
    });
}