module.exports =  function(app, routerConfig){
    var keys = Object.keys(routerConfig);
    
    for(var path of keys) {
        var params = routerConfig[path];
        var ctrl_file = require(params[1]);
        var callback = ctrl_file[params[2]];
        var method = params[0];
        var middleware = params[3];
        if(typeof middleware === 'function')
        {
            app[method](path,middleware,callback);
        }else{
            app[method](path,callback);
        }
    }    
}