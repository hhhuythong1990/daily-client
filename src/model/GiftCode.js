const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const processSkipLimit = require("./../utilities/processSkipLimit");
const processSearch = require("./../utilities/processSearch");

const GiftCodeSchema = mongoose.Schema({
    code:{ //mã code
        type: String,
        required: true
    },
    pin_code: { //mã hiển thị
        type: String,
        required: true
    },
    "agency_package": {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'AgencyPackage',
        required: true
    },
    "agency_package_name": { // tên gói và đại lý
        type: String,
        required: true
    },
    "package_name": { // tên gói
        type: String,
        required: true
    },
    "price_original": { 
        type: Number,
        required: true
    },
    "price_after_cycle": { 
        type: Number,
        required: true
    },
    "serial": { //mã đại lý
        type: String,
        required: true
    },
    "plan_id": { 
        type: Number,
        required: true
    },
    "created_date": {
        type: String,
        required: true
    },
    "expired_date":{
        type: String,
        required: true
    },
    "primitive_data": { // data nhận về từ payment
        type: Object,
        required: true
    },
    "agency": { //đại lý mua gói
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        required: true
    },
    "agency_child": { //đại lý con
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        default: null
    },
    "use_form": { // hình thức mua đầu tiên
        type: String,
        default: null
    },
    "use_form_2": { // hình thức sử dụng của đại lý con
        type: String,
        default: null
    },
    "active_status": { // code đã được kích hoạt
        type: Boolean,
        default: false
    },
    "time_create_epoch":{
        type: Number,
        required: true
    },
    //
    "day_create_date": {
        type: Date,
        required: true
    },
    "day_create_string": {
        type: String,
        required: true
    },
    "day_create_epoch": {
        type: Number,
        required: true
    },
    "user_create": {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        required: true
    },
    "temp": {
        type:  Object,
        default: null
    }
});

const GiftCode = module.exports = mongoose.model("GiftCode", GiftCodeSchema);

module.exports.insert = (newCode, callback) => {
    newCode.save(callback);
}

module.exports.getAll = (skipItem, limitItem, agency, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    let query = { "agency": mongoose.Types.ObjectId(agency) }
    GiftCode.find(query)
        .populate({ "path" : "user_create", "select": "full_name" })
        .populate({ "path" : "user_update", "select": "full_name" })
        .sort({"day_create_date": -1})
        .skip(skip).limit(limit).exec(callback);
}

module.exports.sum = (agency, callback) => {
    let query = { "agency": mongoose.Types.ObjectId(agency) };
    GiftCode.find(query).count(callback);
}

module.exports.takeDataSearchSkipLimit = (skipItem, limitItem, dataSearch, agency, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    let query = {$and: [{"code" : new RegExp(dataSearch, 'i')}, { "agency": mongoose.Types.ObjectId(agency)} ]};
    GiftCode.find(query)
        .populate({ "path" : "user_create", "select": "full_name" })
        .populate({ "path" : "user_update", "select": "full_name" })
        .sort({"day_create_date": -1})
        .skip(skip).limit(limit).exec(callback);
}

module.exports.sumQuery = (dataSearch, agency, callback) => {
    let query = {$and: [{"code" : new RegExp(dataSearch, 'i')}, { "agency": mongoose.Types.ObjectId(agency)} ]};
    GiftCode.find(query).count(callback);
}

module.exports.takeDateSearchSkipLimit = (skipItem, limitItem, dataSearch, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    let query = processSearch.searchGiftCode(dataSearch);
    GiftCode.find(query)
        .sort({"day_create_epoch": -1})
        .skip(skip).limit(limit)
        .exec(callback);
};

module.exports.sumQuery = (dataSearch, callback) => {
    let query = processSearch.searchGiftCode(dataSearch);
    GiftCode.find(query).count(callback);
}

module.exports.takeDataExport = (dataSearch, callback) => {
    let query = processSearch.searchGiftCode(dataSearch);
    GiftCode.find(query)
        .populate({ "path" : "user_create", "select": "full_name" })
        .sort({"time_create_epoch": -1})
        .exec(callback);
};