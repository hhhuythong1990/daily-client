const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const RoleSchema = mongoose.Schema({
    role_name: {
        type: String,
        trim: true,
        required:true    
    },
    permissions: [{
        type:  mongoose.Schema.Types.ObjectId,
        ref: "Permission",
        required:true    
    }],
    date_create: {
        type: Date,
        required: true
    },
    date_update: {
        type: Date,
    },
    date_create_string: {
        type: String,
        required: true
    },
    date_update_string: {
        type: String,
        default: null,
    },
});

const Role = module.exports = mongoose.model("Role", RoleSchema);

module.exports.insert = (newRole, callback) => {
    newRole.save(callback);
}
module.exports.getAll = (skip_item, limit_item, callback) => {
    let limit = parseInt(limit_item);
    let skip = (parseInt(skip_item) - 1) * limit;
    Role.find().populate("permissions").skip(skip).limit(limit).exec(callback);
}
module.exports.sum = (callback) => {
    Role.count(callback);
}
module.exports.deleteById = (Id, callback) => {
    Role.remove({"_id": mongoose.Types.ObjectId(Id)}, callback);
};
module.exports.updateByeId = (Id, dataUpdate, callback) => {
    let query = {"_id": mongoose.Types.ObjectId(Id)};
    Role.update(query, {$set: dataUpdate}, { multi:true}, callback);
};