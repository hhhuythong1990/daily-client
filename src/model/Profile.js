const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const processSkipLimit = require("./../utilities/processSkipLimit");
require("./Permission");
require("./GroupUser");
require("./GroupModule");
require("./Agency");
require("./Profile");
require("./Module");


const ProfileSchema = mongoose.Schema({
    full_name: { // tên người đăng nhập
        type: String,
        trim: true,
        required:true
    }, 
    user_name: { 
        type: String,
        trim: true,
        unique: true,
        required: true
    },
    password: {
        type: String,
        trim: true
    },
    is_locked: {
        type: Boolean,
        default: false
    },
    is_publish: { 
        type: Boolean,
        required: true
    },
    permissions: [{
        type:  mongoose.Schema.Types.ObjectId,
        ref: "Permission",
        required: true    
    }],
    group_user: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: "GroupUser",
        // required: true
    },
    group_parent:[{ // dùng cho các đại lý con/ Admin
        type:  mongoose.Schema.Types.ObjectId,
        ref: "GroupUser",
        default: null    
    }],
    group_modules: [{
        type:  mongoose.Schema.Types.ObjectId,
        ref: "GroupModule",
        required: true    
    }],
    is_agency: { // id của đại lý
        type:  mongoose.Schema.Types.ObjectId,
        ref: "Agency",
        default: null
    },
    is_online: {
        type: Boolean,
        default: false
    },
    is_updated: {
        type: Boolean,
        default: false
    },
    is_changed: {
        type: Boolean,
        default: false
    },
	"log_login" : [{
        type: String,
        default: null
    }],
	"log_logout" :  [{
        type: String,
        default: null
    }],
    day_create_date: {
        type: Date,
        required: true
    },
    day_update_date: {
        type: Date,
    },
    day_create_string: {
        type: String,
        required: true
    },
    day_update_string : {
        type: String,
        default: null
    },
    user_create: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        required: true
    },
    user_update: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        default: null
    },
    temp: {
        type:  Object,
        default: null
    }
});

const Profile = module.exports = mongoose.model("Profile", ProfileSchema);

module.exports.updateByeId = (profileId, dataUpdate, callback) => {
    let query = {"_id": mongoose.Types.ObjectId(profileId)};
    Profile.update(query, {$set: dataUpdate}, { multi:true}).exec(callback);
};

module.exports.takeProfileByUsername = (username, callback) => {
    let query = { $and:[{ "is_agency" : { $ne : null } }, { "user_name": username }]};
    Profile.findOne(query).populate({
        path: "group_modules",
        populate: { path: "modules",        
            populate: { path: "permissions", select: "permission_name_slug"}    
        }
    }).populate({path: "is_agency"}).exec(callback);
}

module.exports.takeProfileById = (profileId, callback) => {
    let query = { $and:[{ "is_agency" : { $ne : null } }, { "_id": profileId }]};
    Profile.findOne(query).populate({
        path: "group_modules",
        populate: { path: "modules",        
            populate: { path: "permissions", select: "permission_name_slug"}    
        }
    }).populate({path: "is_agency"}).exec(callback);
}

module.exports.takeModuleByProfileId = (profileId, callback) => {
    let query = {"_id": mongoose.Types.ObjectId(profileId)};
    Profile.findOne(query).populate({path: "is_agency"}).exec(callback);
};

module.exports.takeProfileChange = (profileId, changeStatus, callback) => {
    let query = {"_id": mongoose.Types.ObjectId(profileId), "is_changed": changeStatus};
    Profile.findOne(query).exec(callback);
};