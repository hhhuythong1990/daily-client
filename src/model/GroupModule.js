const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const processSkipLimit = require("./../utilities/processSkipLimit");
const constants = require("./../utilities/Constants");

const GroupModuleSchema = mongoose.Schema({
    group_name: {
        type: String,
        trim: true,
        required:true
    },
    group_name_slug: {
        type: String,
        trim: true,
        required:true
    },
    modules: [{
        type:  mongoose.Schema.Types.ObjectId,
        ref: "Module",
        required:true    
    }],
    is_publish: {
        type: Boolean,
        required: true
    },
    day_create_date: {
        type: Date,
        required: true
    },
    day_create_string: {
        type: String,
        required: true
    },
    day_update_date: {
        type: Date,
        default: null
    },
    day_update_string : {
        type: String,
        default: null
    },
    user_create: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        required: true
    },
    user_update: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        default: null
    },
    temp: {
        type:  Object,
        default: null
    }
});

const GroupModule = module.exports = mongoose.model("GroupModule", GroupModuleSchema);

module.exports.insertGroupModule = (newModule, callback) => {
    newModule.save(callback);
};

module.exports.takeDataSearchBySkipLimit = (skipItem, limitItem, dataSearch, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    let query = {"group_name" : new RegExp(dataSearch, 'i')};
    GroupModule.find(query)
        .populate("modules", "module_name")
        .populate({ "path" : "user_create", "select": "full_name" })
        .populate({ "path" : "user_update", "select": "full_name" })
        .skip(skip).limit(limit).exec(callback);
};

module.exports.takeGroupModuleBySkipLimit = (skipItem, limitItem, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);    
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    GroupModule.find()
        .populate("modules", "module_name")
        .populate({ "path" : "user_create", "select": "full_name" })
        .populate({ "path" : "user_update", "select": "full_name" })
        .skip(skip).limit(limit).exec(callback);
};

module.exports.sumModules = (callback) => {
    GroupModule.count(callback);
};

module.exports.sumQuery = (dataSearch, callback) => {
    let query = {"group_name" : new RegExp(dataSearch, 'i')};
    GroupModule.find(query).count(callback);
}


module.exports.deleteById = (id, callback) => {
    GroupModule.remove({"_id": mongoose.Types.ObjectId(id)}, callback);
};

module.exports.updateByeId = (Id, dataUpdate, callback) => {
    let query = {"_id": mongoose.Types.ObjectId(Id)};
    GroupModule.update(query, {$set: dataUpdate}, { multi:true}, callback);
};

module.exports.takeAllGroupModule = (callback) => {
    GroupModule.find(callback);
};

module.exports.takeGroupModuleByIdAndGroupName = (id, groupName, callback) => {
    let query = { $and: [{ "_id": { $ne: mongoose.Types.ObjectId(id) } },{ "group_name" : groupName }]};
    GroupModule.findOne(query).exec(callback);
}

module.exports.takeGroupModuleByGroupName = (groupName, callback) => {
    let query = { "group_name" : groupName };
    GroupModule.findOne(query).exec(callback);
}

module.exports.takeGroupModuleByGroupModuleId = (groupModuleId, callback) => {
    let query = {"_id": mongoose.Types.ObjectId(groupModuleId)};
    GroupModule.findOne(query, callback);
};

module.exports.takeGroupModuleShow = (callback) => {
    let query =  {"is_publish": constants.IS_TRUE};
    GroupModule.find(query).exec(callback);
};