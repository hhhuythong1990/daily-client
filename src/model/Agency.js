const mongoose = require("mongoose");
mongoose.Promise = global.Promise;


const AgencySchema = mongoose.Schema({
    agency_code: { // Mã dùng để lấy serial
        type: String,
        trim: true,
        unique: true,
        required:true
    },
    payment_limit: { // hạn mức
        type: Number,
        trim: true,
        required:true    
    },
    full_name_represent: { // tên người đại diện
        type: String,
        trim: true,
        default: null
    },
    id_card: { // số cmnd
        type: String,
        default: null
    },
    phone: { // số điện thoại
        type: String,
        default: null
    },
    email: { 
        type: String,
        default: null
    },
    address: { // địa chỉ
        type: String,
        default: null
    },
    image_id_card_front:{ // ảnh chứng minh mặt trước
        type: String,
        default: null
    },
    image_id_card_behind:{ // ảnh chứng minh mặt sau
        type: String,
        default: null
    },
    password_2: {
        type: String,
        default: null
    },
    temp: {
        type:  Object,
        default: null
    }
});

const Agency = module.exports = mongoose.model("Agency", AgencySchema);

module.exports.insertAgencyInfo = (newAgency, callback) => {
    newAgency.save(callback);
}

module.exports.updateAgencyInfo = (agencyInfoId, dataUpdate, callback) => {
    let query = {"_id": mongoose.Types.ObjectId(agencyInfoId)};
    Agency.update(query, { $set: dataUpdate }, { multi:false }, callback);
};

module.exports.takeAgencyByAgencyCode = (agencyCode, callback) => {
    let query = { "agency_code": agencyCode };
    Agency.findOne(query).exec(callback);
}

module.exports.takeAgencyByIdAndAgencyCode = (id, agencyCode, callback) => {
    let query = { $and:[ { "_id":{$ne: mongoose.Types.ObjectId(id)} },{ "agency_code": agencyCode } ] };
    Agency.findOne(query).exec(callback);
}

module.exports.updateByeId = (agencyId, dataUpdate, callback) => {
    let query = {"_id": mongoose.Types.ObjectId(agencyId)};
    Agency.update(query, {$set: dataUpdate}, { multi:true}).exec(callback);
};

module.exports.takeAgencyById = (id, callback) => {
    let query = { "_id": mongoose.Types.ObjectId(id) };
    Agency.findOne(query).exec(callback);
}