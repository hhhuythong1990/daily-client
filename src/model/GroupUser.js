const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const GroupUserSchema = mongoose.Schema({
    group_user_name: {
        type: String,
        trim: true,
        required:true,
        unique: true    
    },
    group_user_name_slug: {
        type: String,
        trim: true,
        required:true    
    },
    is_publish: {
        type: Boolean,
        required: true
    },
    day_create_date: {
        type: Date,
        required: true
    },
    day_create_string: {
        type: String,
        required: true
    },
    day_update_date: {
        type: Date,
        default: null
    },
    day_update_string : {
        type: String,
        default: null
    },
    user_create: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        default: null
    },
    user_update: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        default: null
    },
    temp: {
        type:  Object,
        default: null
    }
});

const GroupUser = module.exports = mongoose.model("GroupUser", GroupUserSchema);

module.exports.insertGroupUser = (newGroupUser, callback) => {
    newGroupUser.save(callback);
}

module.exports.takeGroupUserByName = (groupUserName, callback) =>{
    const query = {"group_user_name": groupUserName};
    GroupUser.findOne(query, callback);
};

module.exports.takeBySkipLimit = (skip_item, limit_item, callback) => {
    let limit = parseInt(limit_item);
    let skip = (parseInt(skip_item) - 1) * limit;
    GroupUser.find()
        .populate({ "path" : "user_create", "select": "full_name" })
        .populate({ "path" : "user_update", "select": "full_name" })
        .skip(skip).limit(limit).exec(callback);
};

module.exports.sum = (callback) => {
    GroupUser.count(callback);
}
module.exports.deleteById = (Id, callback) => {
    GroupUser.remove({"_id": mongoose.Types.ObjectId(Id)}, callback);
};

module.exports.updateByeId = (Id, dataUpdate, callback) => {
    let query = {"_id": mongoose.Types.ObjectId(Id)};
    GroupUser.update(query, {$set: dataUpdate}, { multi:true}, callback);
};

module.exports.takeAllGroupUser = (callback) => {
    GroupUser.find().exec(callback);
};

module.exports.getGroupAdmin = (id, callback) =>{
    const query = { "_id" : { $ne : mongoose.Types.ObjectId(id) } };
    GroupUser.findOne(query, callback);
};