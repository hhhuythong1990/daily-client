const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const processSkipLimit = require("./../utilities/processSkipLimit");

const AgencyPackageSchema = mongoose.Schema({
    agency_package_name:{
        type: String,
        required:true
    },
    agency: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        required: true
    },
    package: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Package',
        required: true
    },
    cycle: { // chiếc khấu
        type: Number,
        required:true
    },
    price_after_cycle:{
        type: Number,
        required:true
    },
    agency_package_status: {
        type: Boolean,
        required: true
    },
    day_create_date: {
        type: Date,
        required: true
    },
    day_update_date: {
        type: Date,
        default: null
    },
    day_create_string: {
        type: String,
        required: true
    },
    day_update_string: {
        type: String,
        default: null
    },
    user_create: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        required: true
    },
    user_update: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        default: null
    },
    temp: {
        type:  Object,
        default: null
    }
});

const AgencyPackage = module.exports = mongoose.model("AgencyPackage", AgencyPackageSchema);

module.exports.getAll = (skipItem, limitItem, agency, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    let query = {$and: [{ "agency": mongoose.Types.ObjectId(agency) }, {"agency_package_status": true }]}
    AgencyPackage.find(query)
        .populate({
            path: "agency",
            select: "full_name",
            populate: { path: "is_agency"}
        })
        .populate("package")
        .populate({ "path" : "user_create", "select": "full_name" })
        .populate({ "path" : "user_update", "select": "full_name" })
        .skip(skip).limit(limit).exec(callback);
}

module.exports.sum = (agency, callback) => {
    let query = {$and: [{ "agency": mongoose.Types.ObjectId(agency) }, {"agency_package_status": true }]}
    AgencyPackage.find(query).count(callback);
}

module.exports.takeDataSearchSkipLimit = (skipItem, limitItem, dataSearch, agency, callback) => {
    let dataProcess = processSkipLimit(skipItem, limitItem);
    let limit = dataProcess.limit;
    let skip = dataProcess.skip;
    let query = {$and: [{"agency_package_name" : new RegExp(dataSearch, 'i')}, { "agency": mongoose.Types.ObjectId(agency)}, {"agency_package_status": true } ]};
    AgencyPackage.find(query)
        .populate({
            path: "agency", 
            select: "full_name",
            populate: { path: "is_agency"}
        })
        .populate("package")
        .populate({ "path" : "user_create", "select": "full_name" })
        .populate({ "path" : "user_update", "select": "full_name" })
        .skip(skip).limit(limit).exec(callback);
};

module.exports.sumQuery = (dataSearch, agency, callback) => {
    let query = {$and: [{"agency_package_name" : new RegExp(dataSearch, 'i')},  { "agency": mongoose.Types.ObjectId(agency)}, {"agency_package_status": true } ]};
    AgencyPackage.find(query).count(callback);
}

module.exports.takeAgencyPackageById = (id, agency, callback) => {
    let query = {$and: [{"_id": mongoose.Types.ObjectId(id)} , { "agency": mongoose.Types.ObjectId(agency) }, {"agency_package_status": true }]};
    AgencyPackage.findOne(query).populate({
        path: "agency", 
        select: "full_name",
        populate: { path: "is_agency"}
    }).populate("package").exec(callback);
};