const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const processSkipLimit = require("./../utilities/processSkipLimit");
require("./GiftCode");
require("./Profile");
require("./TransactionCode");

const RequestGiftCodeSchema = mongoose.Schema({
    "user_create": { // object đại lý mua gói
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'Profile',
        required: true
    },
    use_form: { // hình thức mua
        type: String,
        default: null
    },
    "agency_name": { // tên đại lý
        type: String,
        required: true
    },
    "agency_package_name": { // tên gói
        type: String,
        required: true
    },
    "old_payment_limit": { // hạn mức trước khi mua gói
        type: Number,
        required: true
    },
    "price_paid": { // số tiền phải trả khi mua gói
        type: Number,
        required: true
    },
    "new_payment_limit": { // hạn mức mới
        type: Number,
        required: true
    },
    "amount_code_request": { // số lượng code yêu cầu tạo
        type: Number,
        required: true
    },
	"amount_code_created" : { // số lượng code nhận về từ payment
        type: Number,
        required: true
    },
    "price_original" : { // giá gốc của gói
        type: Number,
        required: true
    },
    "price_cycle" : { // giá chiết khấu
        type: Number,
        required: true
    },
    "price_after_cycle" : { // giá sau chiết khấu
        type: Number,
        required: true
    },
    "price_original_paid" : { // giá gốc phải trả
        type: Number,
        required: true
    },
    price_paid_difference: { // giá chênh lệch phải trả
        type: Number,
        required: true
    },
    price_difference: { // giá chênh lệch 1 đơn vị
        type: Number,
        required: true
    },
	"gift_code" :  [{ // id code tạo ra
        type:  mongoose.Schema.Types.ObjectId,
        ref: "GiftCode",
        required: true
    }],
    "transaction": { //Mã giao dịch
        type:  String,
        ref: "TransactionCode",
        required: true
    },
    //
    day_create_date: {
        type: Date,
        required: true
    },
    day_create_string: {
        type: String,
        required: true
    },
    time_create_epoch:{
        type: Number,
        required: true
    },
    temp: {
        type:  Object,
        default: null
    }
});

const RequestGiftCode = module.exports = mongoose.model("RequestGiftCode", RequestGiftCodeSchema);

module.exports.insert = (newRequestGiftCode, callback) => {
    newRequestGiftCode.save(callback);
}