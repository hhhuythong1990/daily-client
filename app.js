'use strict';

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require("body-parser");
const expressValidate = require("express-validator");
const morgan = require("morgan");


const config = require("./src/config/" + (process.env["NODE_ENV"] || "production"));

const port = config.PORT;

mongoose.connect(config.DATABASE, { useMongoClient: true });
mongoose.connection.on("connected", () => {
    console.log("Connected to database " + config.DATABASE);
});

mongoose.connection.on("error", (err) => {
    console.log("Database connect error " + err);
});

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(expressValidate());
app.use(morgan("dev"));

const checkAuth = require("./src/utilities/Authentication");
app.use("/", checkAuth);


// const routes = require("./src/http/routes");
// app.use("/",routes);
// app.use("/api",routes);
// app.use("/api", require("./src/controller/ProfileController"));
// app.use("/api", require("./src/controller/ModuleController"));
// app.use("/api", require("./src/controller/GroupModuleController"));
// app.use(express.static(__dirname + '/controller'));

//c1 router
// require('./src/http/routes')(app);
// router.run();
require('./src/http/routes')(app);
app.listen(port, () =>{
    console.log("Server start on port " + port);
});
